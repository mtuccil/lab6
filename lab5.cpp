/*******************
Matthew Tuccillo
mtuccil
Lab 5
CPSC-1021-001
Hollis
*******************/
/*-------------------------------------------------------------
 Description: This program generates and shuffles a virtual
              deck of playing cards and simlates drawing five
              cards. The five cards are then sorted by suit
              and value in ascending order.
              Ex. SPADES < HEARTS < DIAMONDS < CLUBS
-------------------------------------------------------------*/

//Libraries
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"

//Namespace
using namespace std;

//Declare Suit type
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

//Define struct Card
typedef struct Card {
  Suit suit;
  int value;
} Card;

//Prototypes
string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}

//Main
int main(int argc, char const *argv[]) {
  //Seed for random number generator
  srand(unsigned (time(0)));

  //Declare "virtual deck" array
  Card deck[52];
  //Initialize array to contain elements for each
  //   suit & value combination in a deck of cards
  for (int s = 0;s < 4;s++) {
    for (int n = 2;n <= 14;n++) {
      deck[(s*13)+(n-2)].suit = static_cast<Suit>(s);
      deck[(s*13)+(n-2)].value = n;
    }
  }

  //shuffle the "deck of cards" array
  //   randomizing the elements(cards)
  random_shuffle(&deck[0],&deck[52],myrandom);

  //Declare "virtual hand" array
  Card hand[5];
  //"Draw" first 5 cards in "deck" array
  //   set "hand" equal to the cards
  for (int h = 0; h < 5; h++) {
    hand[h].suit = deck[h].suit;
    hand[h].value = deck[h].value;
  }

  //sort all the cards in "hand" in ascending order
  //   by suit then value
  sort(&hand[0],&hand[5],suit_order);

  //print the cards in "hand" array
  for (int c = 0; c < 5; c++) {
    //note:separate lines for easier reading
    cout << setw(10);
    cout << get_card_name(hand[c]);
    cout << " of ";
    cout << get_suit_code(hand[c]);
    cout << endl;
  }
  return 0;
}

/**************************************************************
* function:   bool suit_order(const Card& lhs, const Card& rhs)
*
* explanation: Compares the suits and values of two cards and
*              returns a flag indicating whether or not one of
*              the cards is "greater" or "lesser" than the
*              other.
*
* inputs: Const struct pointer to a card in hand,
*         Const struct pointer to another card in hand
*
* outputs: Bool indicating result of card comparison
*
* details/notes:
*
**************************************************************/
bool suit_order(const Card& lhs, const Card& rhs) {
  //declare bool to hold result
  bool result;
  //if suits are equal, sort by value
  if (lhs.suit == rhs.suit) {
    result = (lhs.value < rhs.value);
  }
  //if suits not equal, sort by suit
  else {
    result = (lhs.suit < rhs.suit);
  }
  return result;
}

/**************************************************************
* function:   string get_suit_code(Card& c)
*
* explanation: Prints symbol corresponding to a specific card's
*              suit.
*
* inputs: Struct pointer to a card to check suit of
*
* outputs: String containing symbol corresponding to card's suit
*
* details/notes: Requires UTF-8 encoding enabled.
*
**************************************************************/
string get_suit_code(Card& c) {
  //check card's suit, print symbol
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

/**************************************************************
* function:   string get_card_name(Card& c)
*
* explanation: Prints value or name of a specific card.
*
* inputs: Struct pointer to a card to check value of
*
* outputs: String representing the value of the input card
*
* details/notes: 2-10 print numerical values
*                ace prints Ace
*                jack prints Jack
*                queen prints Queen
*                king prints King
*
**************************************************************/
string get_card_name(Card& c) {
  switch (c.value) {
    //cards 2-10 print numerical value
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
    case 10:  return to_string(c.value);
    //11 prints Jack
    case 11:  return "Jack";
    //12 prints Queen
    case 12:  return "Queen";
    //13 prints King
    case 13:  return "King";
    //14 prints Ace
    case 14:  return "Ace";
    default:  return "";
  }
}
